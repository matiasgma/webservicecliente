/**
 * DaoEjemploServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package webservice;

public class DaoEjemploServiceLocator extends org.apache.axis.client.Service implements webservice.DaoEjemploService {

    public DaoEjemploServiceLocator() {
    }


    public DaoEjemploServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DaoEjemploServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DaoEjemplo
    private java.lang.String DaoEjemplo_address = "http://localhost:8080/EjemploWebserviceProveedor/services/DaoEjemplo";

    public java.lang.String getDaoEjemploAddress() {
        return DaoEjemplo_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DaoEjemploWSDDServiceName = "DaoEjemplo";

    public java.lang.String getDaoEjemploWSDDServiceName() {
        return DaoEjemploWSDDServiceName;
    }

    public void setDaoEjemploWSDDServiceName(java.lang.String name) {
        DaoEjemploWSDDServiceName = name;
    }

    public webservice.DaoEjemplo getDaoEjemplo() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DaoEjemplo_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDaoEjemplo(endpoint);
    }

    public webservice.DaoEjemplo getDaoEjemplo(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            webservice.DaoEjemploSoapBindingStub _stub = new webservice.DaoEjemploSoapBindingStub(portAddress, this);
            _stub.setPortName(getDaoEjemploWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDaoEjemploEndpointAddress(java.lang.String address) {
        DaoEjemplo_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (webservice.DaoEjemplo.class.isAssignableFrom(serviceEndpointInterface)) {
                webservice.DaoEjemploSoapBindingStub _stub = new webservice.DaoEjemploSoapBindingStub(new java.net.URL(DaoEjemplo_address), this);
                _stub.setPortName(getDaoEjemploWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DaoEjemplo".equals(inputPortName)) {
            return getDaoEjemplo();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice", "DaoEjemploService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice", "DaoEjemplo"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DaoEjemplo".equals(portName)) {
            setDaoEjemploEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
