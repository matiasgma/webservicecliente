package webservice;

public class DaoEjemploProxy implements webservice.DaoEjemplo {
  private String _endpoint = null;
  private webservice.DaoEjemplo daoEjemplo = null;
  
  public DaoEjemploProxy() {
    _initDaoEjemploProxy();
  }
  
  public DaoEjemploProxy(String endpoint) {
    _endpoint = endpoint;
    _initDaoEjemploProxy();
  }
  
  private void _initDaoEjemploProxy() {
    try {
      daoEjemplo = (new webservice.DaoEjemploServiceLocator()).getDaoEjemplo();
      if (daoEjemplo != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)daoEjemplo)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)daoEjemplo)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (daoEjemplo != null)
      ((javax.xml.rpc.Stub)daoEjemplo)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public webservice.DaoEjemplo getDaoEjemplo() {
    if (daoEjemplo == null)
      _initDaoEjemploProxy();
    return daoEjemplo;
  }
  
  public java.lang.String saludar(java.lang.String nombre) throws java.rmi.RemoteException{
    if (daoEjemplo == null)
      _initDaoEjemploProxy();
    return daoEjemplo.saludar(nombre);
  }
  
  
}